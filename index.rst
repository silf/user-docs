.. SILF User documentation documentation master file, created by
   sphinx-quickstart on Thu Aug 16 16:20:31 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SILF User documentation's documentation!
===================================================

Silf is a Remote laboratory extended you can use for free, right now (albeit
reading the manual below is a good idea).

SILF url is: http://silf.stem4youth.pl/.


This project has received funding from the European Union Horizon 2020 Research and Innovation programme under grant agreement no. 710577.


Contents:

.. toctree::
   :maxdepth: 2

   manual/manual.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

